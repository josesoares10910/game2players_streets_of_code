package org.academiadecodigo.streetsofcode.Tiles;

import org.academiadecodigo.streetsofcode.Map.Map;
import org.academiadecodigo.streetsofcode.Player.Player;

public class CheckPlayerCollision {

    private Map map;
    private Player player;
    public boolean isColliding;

    private int player1MinX;
    private int player2Minx;
    private int player1MaxX;
    private int player2MaxX;
    private int player1MinY;
    private int player2MaxY;
    private int player1MaxY;
    private int player2MinY;

    public CheckPlayerCollision(Map map, Player player){
        this.player = player;
        this.map = map;
    }

    public boolean up(Player player){

        updatePositions(player);


        if(player1MinX <= player2MaxX && player1MinX >= player2Minx &&
            player1MinY <= player2MaxY && player1MinY >= player2MinY){
            isColliding = true;

        }
        else if(player1MaxX <= player2MaxX && player1MaxX >= player2Minx &&
                player1MinY <= player2MaxY && player1MinY >= player2MinY){
            isColliding = true;
        } else {
            isColliding = false;
        }
        return isColliding;
    }


    public boolean down(Player player){

        updatePositions(player);


        if((player1MinX <= player2MaxX && player1MinX >= player2Minx) &&
                (player1MaxY <= player2MaxY && player1MaxY >= player2MinY)){
            isColliding = true;

        }
        else if((player1MaxX >= player2Minx && player1MaxX <= player2MaxX) &&
                (player1MaxY <= player2MaxY && player1MaxY >= player2MinY)){
            isColliding = true;
        } else {
            isColliding = false;
        }
        return isColliding;
    }

    public boolean left(Player player){

        updatePositions(player);


        if((player1MinX <= player2MaxX && player1MinX >= player2Minx) &&
                (player1MaxY >= player2MinY && player1MaxY <= player2MaxY)){
            isColliding = true;

        }
        else if((player1MinX <= player2MaxX && player1MinX >= player2Minx) &&
                (player1MinY <= player2MaxY && player1MinY >= player2MinY)){
            isColliding = true;
        } else {
            isColliding = false;
        }
        return isColliding;
    }


    public boolean right(Player player) {

        updatePositions(player);


        if ((player1MaxX <= player2MaxX && player1MaxX >= player2Minx) &&
                (player1MaxY <= player2MaxY && player1MaxY >= player2MinY)) {
            isColliding = true;

        } else if ((player1MaxX <= player2MaxX && player1MaxX >= player2Minx) &&
                (player1MinY <= player2MaxY && player1MinY >= player2MinY)) {
            isColliding = true;
        } else {
            isColliding = false;
        }
        return isColliding;
    }

    public void updatePositions(Player player) {
        player1MinX = this.player.playerTile.getX();
        player2Minx = player.playerTile.getX();
        player1MaxX = this.player.playerTile.getX() + this.player.playerTile.getWidth();
        player2MaxX = player.playerTile.getX() + player.playerTile.getWidth();
        player1MinY = this.player.playerTile.getY();
        player2MaxY = player.playerTile.getY() + player.playerTile.getHeight();
        player1MaxY = this.player.playerTile.getY() + this.player.playerTile.getHeight();
        player2MinY = player.playerTile.getY();
    }
}
