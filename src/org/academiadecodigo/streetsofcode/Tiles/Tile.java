package org.academiadecodigo.streetsofcode.Tiles;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Tile {

    public  Rectangle rectangle;

    public TileType tileType;

    public Picture picture;

    public Tile(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public void setTileType(TileType tileType) {
        this.tileType = tileType;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }
}
