package org.academiadecodigo.streetsofcode.Fight;

import org.academiadecodigo.streetsofcode.Scenes.FightScene;

public class Fight {

    public FightPlayer currentPlayer;
    public FightPlayer[] players = new FightPlayer[2];
    private int baseAttack, baseDefense;
    private FightScene fightScene;

    public int damage;

    public Fight(FightScene fightScene) {
        this.fightScene = fightScene;
        players[0] = new FightPlayer(this, 100, 25, 20, "player1", fightScene.fightUI, ActionType.NONE);
        players[1] = new FightPlayer(this, 100, 25, 20, "player2", fightScene.fightUI, ActionType.NONE);
        baseAttack = 25;
        baseDefense = 20;
        damage = 0;
    }

    public boolean isFinished(){
        if(players[0].health <= 0 || players[1].health <= 0){
            return true;
        }
        return false;
    }

    public void setAction(ActionType action, int player){
        if(action == ActionType.NONE){
            throw new RuntimeException("Invalid action"); // não é suposto acontecer o player não ter nenhuma action
        }


        switch (player){
            case 1:
                players[0].actionType = action;
                currentPlayer = players[1];
                break;

            case 2:
                players[1].actionType = action;
                currentPlayer = players[0];
                break;

            default:
                throw new RuntimeException("Invalid player");
        }
    }

    public boolean checkRound(){

        return true;
    }


    public void fight(){
        int attackValue = baseAttack + (int) (Math.random() * 21 - 10);// tirar um número random entre 10 e -10
        // no máximo o attack vai ser 60 e min vai ser 40
        int defenseValue = baseDefense + (int) (Math.random() * 11 - 5); // -5 e 5

        if(players[0].actionType == ActionType.ATTACK1 && players[1].actionType == ActionType.ATTACK1){
            int attackValuePlayer2 = baseAttack + (int) (Math.random() * 21 - 10);

            players[0].health -= attackValuePlayer2;
            players[1].health -= attackValue;

            System.out.println(attackValue);
            System.out.println(attackValuePlayer2);

            System.out.println("PLayer 1 ficou " + players[0].health);
            System.out.println("PLayer 2 ficou " + players[1].health);

        }else if(players[0].actionType == ActionType.ATTACK1 &&  players[1].actionType == ActionType.DEFEND){
            damage = attackValue - defenseValue;

            players[1].health -= damage;

            System.out.println("2 opção, player 2 ficou " + players[1].health);

        } else if (players[0].actionType == ActionType.DEFEND && players[1].actionType == ActionType.ATTACK1) {
            damage = attackValue - defenseValue;

            players[0].health -= damage;

            System.out.println("3 opção, player 1 ficou " + players[0].health);

        }

        players[0].actionType = ActionType.NONE;
        players[1].actionType = ActionType.NONE;

    }

    //region old fight code
    /*





    public int getCurrentPlayer(){
        return currentPlayer;
    }


    public boolean isRoundReady(){
        if(fightActionPlayer1 != ActionType.NONE && fightActionPlayer2 != ActionType.NONE){
            return true;
        }
        return false;
    }





     */
    //endregion

}
