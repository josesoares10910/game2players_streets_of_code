package org.academiadecodigo.streetsofcode.Fight;

import org.academiadecodigo.simplegraphics.mouse.Mouse;
import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.simplegraphics.mouse.MouseEventType;
import org.academiadecodigo.simplegraphics.mouse.MouseHandler;


public class HandlerMouse implements MouseHandler {

    private Mouse mouse;

    private MouseEvent event;

    public HandlerMouse() {
        mouse = new Mouse(this);
        this.mouse.addEventListener(MouseEventType.MOUSE_CLICKED);

    }

    @Override
    public void mouseClicked(MouseEvent event) {
        System.out.println("X: " + event.getX() + "Y: " + event.getY());
    }



    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
    }

}
