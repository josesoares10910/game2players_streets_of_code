package org.academiadecodigo.streetsofcode.Fight;

import org.academiadecodigo.streetsofcode.Player.KeyHandler;
import org.academiadecodigo.streetsofcode.UI.FightUI;

public class FightPlayer {

    private Fight fight;
    public String name;
    public int health;
    public int damage;

    public int previousHealth;

    public int baseAttack, baseDefense, attack, defense;
    public KeyHandler keyHandler;

    public ActionType actionType;
    FightUI fightUI;

    public FightPlayer(Fight fight, int health, int baseAttack, int baseDefense, String name, FightUI fightUI, ActionType actionType){
        this.fight = fight;
        this.health = health;
        previousHealth = health;
        this.baseAttack = baseAttack;
        this.baseDefense = baseDefense;
        attack = baseAttack;
        defense = baseDefense;
        this.name = name;
        this.fightUI = fightUI;
        this.actionType = actionType;
        keyHandler = new KeyHandler(name);
        damage = 0;
    }

    public ActionType doAction(ActionType actionType, int player) {




        switch (actionType){
            case NONE:
                break;
            case ATTACK1:
                damage = attack - fight.players[player].defense / 2;
                fight.players[player].health -= damage;
               break;
            case ATTACK2:
                damage = attack - fight.players[player].defense / 4;
                fight.players[player].health -= damage;
                break;
            case DEFEND:
                defense += 10;
                damage = 0;
                break;
            case RANDOM:
                System.out.println("RANDOM");
                break;
        }

        return actionType;

    }





}
