package org.academiadecodigo.streetsofcode.Fight;

public enum ActionType {

    NONE,
    ATTACK1,
    ATTACK2,
    DEFEND,
    RANDOM

}
