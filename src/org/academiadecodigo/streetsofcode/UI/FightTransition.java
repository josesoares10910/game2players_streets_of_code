package org.academiadecodigo.streetsofcode.UI;

import org.academiadecodigo.streetsofcode.Map.Field;
import org.academiadecodigo.streetsofcode.Player.Direction;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class FightTransition {

    public Rectangle[][] transitionSquares;
    public Direction direction;
    private int col = 0;
    private int row = 0;
    private int maxCol = Field.fieldMaxCol;
    private int maxRow = Field.fieldMaxRow;
    private int minCol = -1;
    private int minRow = -1;

    public FightTransition() {
        transitionSquares = new Rectangle[Field.fieldMaxRow][Field.fieldMaxCol];

        for(int i = 0; i < transitionSquares.length; i++) {
            for(int j = 0; j < transitionSquares[i].length; j++) {
                transitionSquares[i][j] = new Rectangle(j*Field.tileSize+10, i*Field.tileSize+10, Field.tileSize, Field.tileSize);
                transitionSquares[i][j].setColor(Color.BLACK);
            }
        }



        direction = Direction.RIGHT;

    }

    public boolean startTransition() {


        if(col != Field.fieldMaxCol/2 || row != Field.fieldMaxRow/2) {
            if(col >= maxCol-1 && direction == Direction.RIGHT) {
                maxCol--;
                direction = Direction.DOWN;
            } else if(row >= maxRow-1 && direction == Direction.DOWN) {
                maxRow--;
                direction = Direction.LEFT;
            } else if(col <= minCol+1 && direction == Direction.LEFT) {
                minCol++;
                direction = Direction.UP;
            } else if(row <= minRow + 1 && direction == Direction.UP) {
                minRow++;
                direction = Direction.RIGHT;
            }

            switch(direction) {
                case UP:
                    row--;
                    break;
                case DOWN:
                    row++;
                    break;
                case LEFT:
                    col--;
                    break;
                case RIGHT:
                    col++;
                    break;
            }
            return true;
        }
        return false;
    }

    public void deleteTiles() {
        for(int i = 0; i < transitionSquares.length; i++) {
            for(int j = 0; j < transitionSquares[i].length; j++) {
                transitionSquares[i][j].delete();
            }
        }
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }


}
