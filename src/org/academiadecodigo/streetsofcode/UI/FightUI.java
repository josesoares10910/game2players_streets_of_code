package org.academiadecodigo.streetsofcode.UI;

import org.academiadecodigo.streetsofcode.Fight.ActionType;
import org.academiadecodigo.streetsofcode.Fight.FightPlayer;
import org.academiadecodigo.streetsofcode.Map.Field;
import org.academiadecodigo.streetsofcode.Scenes.FightScene;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class FightUI {

    private FightScene fightScene;
    private Rectangle buttonBackground, button1, button2, button3, button4, descriptionBackground, health1Padding, health2Padding;
    public Rectangle player1Health, player2Health;

    public Text button1Text, button2Text, button3Text, button4Text;
    public Text turnText;

    public Text player1HealthText, player2HealthText, player1DefenseText, player2DefenseText, playerMoveText, playerStatusText;

    public Text exitText;

    public Picture[] fightButtons;
    private Picture fightMonitor;
    private Picture fightBackground;
    public Picture[] playerPictures;
    public Picture player1Picture;
    public Picture player2Picture;

    public Picture player1Power, player2Power;

    public Picture playerWon;

    private Picture player1Name;
    private Picture player2Name;
    private int buttonHeight = Field.fieldHeight/6-15;
    private int buttonWidth = Field.fieldWidth/4-10;
    private int buttonsX = 20;
    private int buttonsY = 20 + Field.fieldHeight/3*2;

    private int padding = 10;
    private int descriptionX = buttonsX + padding + buttonWidth + buttonWidth + padding;
    private int descriptionY = buttonsY;
    private int descriptionWidth = Field.fieldWidth - padding - descriptionX;
    private int descriptionHeight = buttonHeight*2 + padding;

    private int healthWidth = Field.tileSize*6;
    private int healthHeight = Field.tileSize;

    private int counter;
    private int originalCounter;
    private int player1Hashcode;
    private int player2Hashcode;

    public FightUI(FightScene fightScene) {

        this.fightScene = fightScene;
        player1Hashcode = fightScene.fight.players[0].hashCode();
        player2Hashcode = fightScene.fight.players[1].hashCode();
        fightBackground = new Picture(padding, padding, "/fightSprites/fight_background.png");

        turnText = new Text(Field.fieldWidth/2, Field.tileSize*2, "player1 turn");

        exitText = new Text(Field.fieldWidth-Field.tileSize*5, Field.fieldHeight-Field.tileSize*1, "");

        exitText.setColor(Color.BLACK);

        player1Picture = new Picture(Field.fieldWidth - buttonWidth, 180, "/fightSprites/bri_posicao1.png");
        player2Picture = new Picture(buttonWidth/2+15, 155, "/fightSprites/jedi_nuno.png");
        playerPictures = new Picture[]{player1Picture, player2Picture};
        player1Power = new Picture(player1Picture.getX(), player1Picture.getY(), "/fightSprites/baby_attack.png");
        player2Power = new Picture(player2Picture.getX()+player2Picture.getWidth(), player2Picture.getY(), "/fightSprites/lightning_image.png");

        buttonBackground = new Rectangle(padding, 10 + Field.fieldHeight/3*2, Field.fieldWidth-10, Field.fieldHeight/3);
        button1 = new Rectangle(buttonsX, buttonsY, buttonWidth, buttonHeight);
        button2 = new Rectangle(buttonsX, buttonsY + buttonHeight + padding, buttonWidth, buttonHeight);
        button3 = new Rectangle(buttonsX + padding + buttonWidth, buttonsY, buttonWidth, buttonHeight);
        button4 = new Rectangle(buttonsX + padding + buttonWidth, buttonsY + buttonHeight + padding, buttonWidth, buttonHeight);
        descriptionBackground = new Rectangle(descriptionX, descriptionY, descriptionWidth, descriptionHeight);
        fightMonitor = new Picture(descriptionX, descriptionY, "/fightSprites/iMac_fight.png");

        player1HealthText = new Text(fightMonitor.getX() + padding, fightMonitor.getY() + padding, "Player1 Health: ");
        player2HealthText = new Text(fightMonitor.getX() + padding, fightMonitor.getY() + padding + player1HealthText.getWidth() + padding, "Player2 Health: ");
        player1DefenseText = new Text(fightMonitor.getX() + padding*24 + player1HealthText.getWidth(), fightMonitor.getY() + padding, "Player1 Defense: ");
        player2DefenseText = new Text(fightMonitor.getX() + padding*24 + player1HealthText.getWidth(), fightMonitor.getY() + padding + player1HealthText.getWidth() + padding, "Player2 Defense: ");
        playerMoveText = new Text(fightMonitor.getX() + padding, fightMonitor.getY()+fightMonitor.getHeight()/3*2, "");
        playerStatusText = new Text(fightMonitor.getX() + padding, fightMonitor.getY()+fightMonitor.getHeight(), "");

        player2Health = new Rectangle(Field.fieldWidth - Field.tileSize - healthWidth, Field.tileSize + padding, healthWidth, healthHeight);
        health2Padding = new Rectangle(Field.fieldWidth - Field.tileSize - healthWidth - padding, Field.tileSize, healthWidth + padding*2, healthHeight + padding*2);
        player1Health = new Rectangle(Field.tileSize + padding, Field.tileSize + padding, healthWidth, healthHeight);
        health1Padding = new Rectangle(Field.tileSize, Field.tileSize, healthWidth + padding*2, healthHeight + padding*2);

        button1Text = new Text(buttonsX + buttonWidth/2, buttonsY + buttonHeight/2, "1: Power Attack");
        button2Text = new Text(buttonsX + buttonWidth/2, buttonsY + buttonHeight + padding + buttonHeight/2, "2: Simple Attack" );
        button3Text = new Text(buttonsX + padding + buttonWidth + buttonWidth/2, buttonsY + buttonHeight/2, "3: Defense");
        button4Text = new Text(buttonsX + padding + buttonWidth + buttonWidth/2, buttonsY + padding + buttonHeight + buttonHeight/2, "4: Slack Around");

        fightButtons = new Picture[4];
        fightButtons[0] = new Picture(button1.getX(), button1.getY(), "/fightSprites/black_button.png");
        fightButtons[1] = new Picture(button2.getX(), button2.getY(), "/fightSprites/blue_button.png");
        fightButtons[2] = new Picture(button3.getX(), button3.getY(), "/fightSprites/red_button.png");
        fightButtons[3] = new Picture(button4.getX(), button4.getY(), "/fightSprites/green_button.png");

        player1Name = new Picture(Field.tileSize, health1Padding.getY()+health1Padding.getHeight()+10, "/fightSprites/jedi_nuno_name.png");
        player2Name = new Picture(health2Padding.getX()+ health2Padding.getWidth(), health1Padding.getY()+health1Padding.getHeight()+10, "/fightSprites/jedi_bri_name.png");

        playerWon = new Picture(Field.fieldWidth/2, Field.fieldHeight/2, "/fightSprites/player_1_won.png");
        playerWon.translate(-playerWon.getWidth()/2, -playerWon.getHeight()/2);

    }

    /*
    Buttons for fight options : 4 buttons
    Health Bars : 2 bars
    Attack Scenes : ?
    Text for everything :
        Player names
        Buttons with descriptions
        Health values
        Action descriptions throughout fight
     */


    public void load() {
        System.out.println("Width: " + descriptionBackground.getWidth() + ", Height: " + descriptionBackground.getHeight());
        System.out.println(Field.fieldHeight-10 - buttonBackground.getHeight());
        buttonBackground.setColor(Color.BLACK);
        buttonBackground.fill();
        button1.setColor(Color.CYAN);
        //button1.fill();
        button2.setColor(Color.CYAN);
        //button2.fill();
        button3.setColor(Color.CYAN);
        //button3.fill();
        button4.setColor(Color.CYAN);
        //button4.fill();
        descriptionBackground.setColor(Color.GREEN);
        //descriptionBackground.fill();
        fightMonitor.grow(10, 2);
        fightMonitor.translate(10, 2);
        fightMonitor.draw();



        player1HealthText.setText("Player1 Health: " + fightScene.fight.players[1].health);
        player1HealthText.grow(40, 20);
        player1HealthText.translate(60, 20);
        player1HealthText.setColor(Color.WHITE);
        player2HealthText.setText("Player2 Health: " + fightScene.fight.players[0].health);
        player2HealthText.grow(40, 20);
        player2HealthText.translate(60, -40);
        player2HealthText.setColor(Color.WHITE);

        player1HealthText.draw();
        player2HealthText.draw();

        player1DefenseText.setText("Player1 Defense: " + fightScene.fight.players[1].defense);
        player1DefenseText.translate(-60, 0);
        player1DefenseText.grow(40, 20);
        player1DefenseText.translate(-30, 20);

        player1DefenseText.setColor(Color.WHITE);
        player2DefenseText.setText("Player2 Defense: " + fightScene.fight.players[0].defense);
        player2DefenseText.translate(-60, 0);
        player2DefenseText.grow(40, 20);
        player2DefenseText.translate(-30, -40);

        player2DefenseText.setColor(Color.WHITE);

        player1DefenseText.draw();
        player2DefenseText.draw();

        playerMoveText.setText("");
        playerMoveText.grow(80, 20);
        playerMoveText.translate(100, -40);
        playerMoveText.setColor(Color.WHITE);

        playerMoveText.draw();

        playerStatusText.setText("");
        playerStatusText.grow(60, 20);
        playerStatusText.translate(80, -80);
        playerStatusText.setColor(Color.WHITE);

        playerStatusText.draw();



        fightBackground.grow(0, 6);
        fightBackground.translate(0, 6);
        fightBackground.draw();

        health1Padding.fill();
        player1Health.setColor(Color.RED);
        player1Health.fill();
        health2Padding.setColor(Color.BLACK);
        health2Padding.fill();
        player2Health.setColor(Color.RED);
        player2Health.fill();

        player1Name.draw();
        player2Name.translate(-player2Name.getWidth(), 0);
        player2Name.draw();


        player1Picture.draw();
        //player2Picture.grow(15, 25);
        player2Picture.translate(0, -25);
        player2Picture.draw();

        System.out.println("Player1 Width: " + player1Picture.getWidth());
        System.out.println("Player1 Height: " + player1Picture.getHeight());

        System.out.println("Player2 Width: " + player2Picture.getWidth());
        System.out.println("Player2 Height: " + player2Picture.getHeight());
        button1Text.translate(-button1Text.getWidth()/2, -button1Text.getHeight() /2);
        button2Text.translate(-button2Text.getWidth()/2, -button2Text.getHeight()/2);
        button3Text.translate(-button3Text.getWidth()/2, -button3Text.getHeight()/2);
        button4Text.translate(-button4Text.getWidth()/2, -button4Text.getHeight()/2);

        turnText.grow(60, 20);
        turnText.setColor(Color.DARK_GRAY);
        turnText.draw();


        for(int i = 0; i < fightButtons.length; i++){
            fightButtons[i].grow(-3, 0);
            fightButtons[i].translate(-3,0);
            fightButtons[i].draw();
        }

        button1Text.setColor(Color.WHITE);
        //button1Text.grow(30, 15);
        button2Text.setColor(Color.WHITE);
        //button2Text.grow(30, 15);
        button3Text.setColor(Color.WHITE);
        //button3Text.grow(30, 15);
        button4Text.setColor(Color.WHITE);
        //button4Text.grow(30, 15);

        button1Text.grow(55, 15);
        button1Text.draw();
        button2Text.grow(55, 18);
        button2Text.translate(30, 0);
        button2Text.draw();
        button3Text.grow(55, 15);
        button3Text.translate(-5, 0);
        button3Text.draw();
        button4Text.grow(52, 15);
        button4Text.translate(2, 0);
        button4Text.draw();


    }

    public void animate(ActionType actionType){

        switch (actionType){
            case ATTACK1:
                simpleAttack(fightScene.fight.players[fightScene.currentPlayer]);
                break;
            case ATTACK2:
                strongAttack(fightScene.fight.players[fightScene.currentPlayer]);
                break;
            case DEFEND:
                defend(fightScene.fight.players[fightScene.currentPlayer]);
                break;
            case RANDOM:
                random(fightScene.fight.players[fightScene.currentPlayer]);
                break;
            case NONE:
                break;
        }

    }

    public void simpleAttack(FightPlayer player){
        if(counter == 0) {
            originalCounter = 48;
            counter = originalCounter;
        }

        if(player.hashCode() == player1Hashcode){

            if(counter == 48) {
                playerMoveText.setText("Player2 attacked with a punchy punch!");
                playerStatusText.setText("Player1 got damaged by " + fightScene.fight.players[0].damage);
            }
            if(counter > 24) {
                player1Picture.load("/fightSprites/bri_posicao2.png");
                player1Picture.translate(-16, 0);

            }
            if(counter <= 24) {
                player1Picture.load("/fightSprites/bri_posicao1.png");
                player1Picture.translate(16, 0);

            }
            counter --;
            if(counter == 0){
                player1Picture.load("/fightSprites/bri_posicao1.png");
                player1Picture.draw();


                fightScene.animationIsOver = true;
            }
            player1Picture.draw();
        } else if (player.hashCode() == player2Hashcode) {
            if(counter == 48) {
                player2Picture.grow(60, 0);
                player2Picture.translate(0, 40);
                playerMoveText.setText("Player1 attacked with a lightsaber!");
                playerStatusText.setText("Player2 got damaged by " + fightScene.fight.players[1].damage);
            }

            if(counter > 24) {
                player2Picture.load("/fightSprites/jedi_nuno_attack.png");
                player2Picture.translate(16, 0);
            }
            if(counter == 24) {
                player2Picture.translate(0, -40);
                player2Picture.grow(-60, 0);
            }
            if(counter <= 24) {
                player2Picture.load("/fightSprites/jedi_nuno.png");
                player2Picture.translate(-16, 0);
            }
            counter --;
            if(counter == 0){
                player2Picture.load("/fightSprites/jedi_nuno.png");
                player2Picture.draw();

                fightScene.animationIsOver = true;
            }
            player2Picture.draw();
        }
    }

    public void strongAttack(FightPlayer player){
        if(player.hashCode() == player1Hashcode){
            if(counter == 0){
                originalCounter = 24;
                counter = originalCounter;
                player1Power.grow(-80, -80);
                player1Power.draw();
                playerMoveText.setText("Player2 threw a baby to player1!");
                playerStatusText.setText("Player1 got damaged by " + fightScene.fight.players[0].damage);
            }
            player1Picture.load("/fightSprites/bri_posicao2.png");
            counter --;
            if(counter >= 12) {
                player1Power.grow(5, 5);
                player1Power.translate(-25, -10);
            }
            if(counter < 12) {
                player1Power.grow(-5, -5);
                player1Power.translate(-25, 10);
            }
            if(counter == 0){
                player1Picture.load("/fightSprites/bri_posicao1.png");
                player1Picture.draw();
                fightScene.animationIsOver = true;
                player1Power.translate(25*24, 0);
                player1Power.grow(80, 80);
                player1Power.delete();
            }
            player1Picture.draw();
        } else if (player.hashCode() == player2Hashcode) {
            if(counter == 0){
                counter = 24;
                player2Power.draw();
                playerMoveText.setText("Player1 threw lightning at player2!");
                playerStatusText.setText("Player2 got damaged by " + fightScene.fight.players[1].damage);
            }
            if(counter == 24) {
                player2Picture.grow(60, 0);
                player2Picture.translate(0, 40);
            }
            player2Picture.load("/fightSprites/jedi_nuno_attack.png");
            counter --;
            if(counter == 0){
                player2Picture.load("/fightSprites/jedi_nuno.png");
                player2Picture.translate(0, -40);
                player2Picture.grow(-60, 0);
                player2Picture.draw();
                fightScene.animationIsOver = true;
                player2Power.delete();
            }
            player2Picture.draw();
        }
    }

    public void defend(FightPlayer player){
        if(counter == 0) counter = 48;

        if(player.hashCode() == player1Hashcode){

            if(counter == 48) {
                playerMoveText.setText("Player2 upgraded defense!");
                playerStatusText.setText("Player 2 now has 10 more defense points!");
            }
            counter --;

            if(counter == 0){

                player1Picture.draw();
                fightScene.animationIsOver = true;
            }
            if(counter > 24){
                player1Picture.translate(4, 0);

            }

            if(counter <= 24){
                player1Picture.translate(-4,0);
            }


            player1Picture.draw();
        } else if (player.hashCode() == player2Hashcode) {
            if(counter == 48) {
                playerMoveText.setText("Player1 upgraded defense!");
                playerStatusText.setText("Player1 now has 10 more defense points!");
            }
            counter --;
            if(counter == 0){

                player2Picture.draw();
                fightScene.animationIsOver = true;
            }
            if(counter > 24){
                player2Picture.translate(-4, 0);

            }

            if(counter <= 24){
                player2Picture.translate(4,0);
            }
            player2Picture.draw();
        }
    }

    public void random(FightPlayer player){
        if(counter == 0) counter = 24;

        if(player.hashCode() == player1Hashcode){
            if(counter == 24) {
                playerMoveText.setText("Absolutely nothing happened");
                playerStatusText.setText("It just looks like it did");
            }

            player1Picture.load("/fightSprites/bri_posicao2.png");
            counter --;
            if(counter == 0){
                player1Picture.load("/fightSprites/bri_posicao1.png");
                player1Picture.draw();
                fightScene.animationIsOver = true;
            }
            player1Picture.draw();
        } else if (player.hashCode() == player2Hashcode) {
            if(counter == 24) {
                player2Picture.grow(60, 0);
                player2Picture.translate(0, 40);
                playerMoveText.setText("Absolutely nothing happened");
                playerStatusText.setText("It just looks like it did");
            }
            player2Picture.load("/fightSprites/jedi_nuno_attack.png");
            counter --;
            if(counter == 0){
                player2Picture.load("/fightSprites/jedi_nuno.png");
                player2Picture.translate(0, -40);
                player2Picture.grow(-60, 0);
                player2Picture.draw();
                fightScene.animationIsOver = true;
            }
            player2Picture.draw();
        }
    }

    public void updateHealth(FightPlayer player) {

        if(player.hashCode() == player1Hashcode) {
            player2Health.grow(-(fightScene.fight.players[fightScene.currentPlayer].damage), 0);
            player2Health.translate((fightScene.fight.players[fightScene.currentPlayer].damage), 0);
        } else if(player.hashCode() == player2Hashcode) {
            player1Health.grow(-(fightScene.fight.players[fightScene.currentPlayer].damage), 0);
            player1Health.translate(-(fightScene.fight.players[fightScene.currentPlayer].damage), 0);
        }
    }

    public void unload(){
        buttonBackground.delete();
        button1.delete();
        button2.delete();
        button3.delete();
        button4.delete();
        button1Text.delete();
        button2Text.delete();
        button3Text.delete();
        button4Text.delete();
        descriptionBackground.delete();
        health1Padding.delete();
        player1Health.delete();
        health2Padding.delete();
        player2Health.delete();
        fightBackground.delete();
        player1Picture.delete();
        player2Picture.delete();
        for(int i = 0; i < playerPictures.length; i++) {
            playerPictures[i].delete();
        }

        for(int i = 0; i < fightButtons.length; i++) {
            fightButtons[i].delete();
        }
        fightMonitor.delete();
        player1Name.delete();
        player2Name.delete();
    }
}
