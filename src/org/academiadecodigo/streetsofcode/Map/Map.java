package org.academiadecodigo.streetsofcode.Map;

import org.academiadecodigo.streetsofcode.Tiles.Tile;
import org.academiadecodigo.streetsofcode.Tiles.TileType;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.io.*;

public class Map {

    public Tile[][] tiles;

    public Map(){
        tiles = new Tile [Field.fieldMaxRow][Field.fieldMaxCol];
    }

    public void loadMap() throws FileNotFoundException {

        //Scanner scanner = new Scanner(new File("res/maps/map1.txt"));

        int row = 0;
        int col = 0;

        int[][] intArray = new int[Field.fieldMaxRow][Field.fieldMaxCol];

        String[][] stringArray = new String[Field.fieldMaxRow][Field.fieldMaxCol];

        try{

            InputStream is = getClass().getResourceAsStream("/maps/map1.txt");

            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            while(row < Field.fieldMaxRow){
                stringArray[row] = br.readLine().split(" ");
                row++;
            }
            br.close();

        } catch(Exception e) {
        }

        row = 0;

        while(row < Field.fieldMaxRow){
            intArray[row][col] = Integer.parseInt(stringArray[row][col]);
            col ++;
            if(col >= Field.fieldMaxCol){
                row++;
                col = 0;
            }
        }

        for(int i = 0; i < intArray.length; i ++){
            for(int j = 0; j < intArray[i].length; j++){
                tiles[i][j] = new Tile(new Rectangle(10 + j * Field.tileSize, 10 + i * Field.tileSize, Field.tileSize, Field.tileSize));

                Tile tile = tiles[i][j];

                int tileType = intArray[i][j];

                TileType type = TileType.fromValue(tileType);
                tile.setTileType(type);
                tile.picture = new Picture(tile.rectangle.getX(), tile.rectangle.getY(), tile.tileType.getFile());
                tile.picture.draw();

            }
        }
    }



}
