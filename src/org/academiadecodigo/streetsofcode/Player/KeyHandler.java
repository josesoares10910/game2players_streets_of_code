package org.academiadecodigo.streetsofcode.Player;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class KeyHandler implements KeyboardHandler {

    public boolean up, down, left, right;
    public boolean button1, button2, button3, button4;
    public boolean pause, resume, fight;

    Keyboard keyboard;
    KeyboardEvent[] events;



    int[] eventKeys;

    public KeyHandler(String playerS) {
        keyboard = new Keyboard(this);
        events = new KeyboardEvent[9];
        eventKeys = new int[9];
        createEvents(playerS);
    }

    public void createEvents(String playerS) {
        for(int i = 0; i < events.length; i++) {
            events[i] = new KeyboardEvent();
        }

        if(playerS.equals("player1")){
            System.out.println("Created keys for player1");
            events[0].setKey(KeyboardEvent.KEY_UP);
            eventKeys[0] = KeyboardEvent.KEY_UP;
            events[1].setKey(KeyboardEvent.KEY_DOWN);
            eventKeys[1] = KeyboardEvent.KEY_DOWN;
            events[2].setKey(KeyboardEvent.KEY_LEFT);
            eventKeys[2] = KeyboardEvent.KEY_LEFT;
            events[3].setKey(KeyboardEvent.KEY_RIGHT);
            eventKeys[3] = KeyboardEvent.KEY_RIGHT;
            events[4].setKey(KeyboardEvent.KEY_7);
            eventKeys[4] = KeyboardEvent.KEY_7;
            events[5].setKey(KeyboardEvent.KEY_8);
            eventKeys[5] = KeyboardEvent.KEY_8;
            events[6].setKey(KeyboardEvent.KEY_9);
            eventKeys[6] = KeyboardEvent.KEY_9;
            events[7].setKey(KeyboardEvent.KEY_0);
            eventKeys[7] = KeyboardEvent.KEY_0;
            events[8].setKey(32);
            eventKeys[8] = 32;

        } else if (playerS.equals("player2")){
            System.out.println("Created keys for player2");
            events[0].setKey(KeyboardEvent.KEY_W);
            eventKeys[0] = KeyboardEvent.KEY_W;
            events[1].setKey(KeyboardEvent.KEY_S);
            eventKeys[1] = KeyboardEvent.KEY_S;
            events[2].setKey(KeyboardEvent.KEY_A);
            eventKeys[2] = KeyboardEvent.KEY_A;
            events[3].setKey(KeyboardEvent.KEY_D);
            eventKeys[3] = KeyboardEvent.KEY_D;
            events[4].setKey(KeyboardEvent.KEY_1);
            eventKeys[4] = KeyboardEvent.KEY_1;
            events[5].setKey(KeyboardEvent.KEY_2);
            eventKeys[5] = KeyboardEvent.KEY_2;
            events[6].setKey(KeyboardEvent.KEY_3);
            eventKeys[6] = KeyboardEvent.KEY_3;
            events[7].setKey(KeyboardEvent.KEY_4);
            eventKeys[7] = KeyboardEvent.KEY_4;
            events[8].setKey(27);
            eventKeys[8] = 27;
        }
        /*
        else if(playerS.equals("fightPlayer1")) {
            System.out.println("Created keys for Fight player1");
            events[0].setKey(KeyboardEvent.KEY_Q);
            eventKeys[0] = KeyboardEvent.KEY_Q;
            events[1].setKey(KeyboardEvent.KEY_W);
            eventKeys[1] = KeyboardEvent.KEY_W;
            events[2].setKey(KeyboardEvent.KEY_A);
            eventKeys[2] = KeyboardEvent.KEY_A;
            events[3].setKey(KeyboardEvent.KEY_S);
            eventKeys[3] = KeyboardEvent.KEY_S;
        } else if(playerS.equals("fightPlayer2"))
        System.out.println("Created keys for Fightplayer2");
        events[0].setKey(KeyboardEvent.KEY_I);
        eventKeys[0] = KeyboardEvent.KEY_I;
        events[1].setKey(KeyboardEvent.KEY_O);
        eventKeys[1] = KeyboardEvent.KEY_O;
        events[2].setKey(KeyboardEvent.KEY_K);
        eventKeys[2] = KeyboardEvent.KEY_K;
        events[3].setKey(KeyboardEvent.KEY_L);
        eventKeys[3] = KeyboardEvent.KEY_L;

         */


        for(int i = 0; i < events.length; i++) {
            events[i].setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboard.addEventListener(events[i]);
        }

        for (int i = 0; i < events.length; i++) {
            KeyboardEvent evt = new KeyboardEvent();
            evt.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
            evt.setKey(events[i].getKey());
            keyboard.addEventListener(evt);
        }
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        if (keyboardEvent.getKey() == eventKeys[8]) {
            System.exit(0);
        }
        if (keyboardEvent.getKey() == eventKeys[0]) {
            up = true;
        } if (keyboardEvent.getKey() == eventKeys[1]) {
            down = true;
        } if (keyboardEvent.getKey() == eventKeys[2]) {
            left = true;
        } if (keyboardEvent.getKey() == eventKeys[3]) {
            right = true;
        }

        if (keyboardEvent.getKey() == eventKeys[4]) {
            button1 = true;
        } if (keyboardEvent.getKey() == eventKeys[5]) {
            button2 = true;
        } if (keyboardEvent.getKey() == eventKeys[6]) {
            button3 = true;
        } if (keyboardEvent.getKey() == eventKeys[7]) {
            button4 = true;
        }

        /*
        if(keyboardEvent.getKey() == eventKeys[4]) {
            pause = true;
        }

        if(keyboardEvent.getKey() == eventKeys[5]) {
            resume = true;
        }

        if (keyboardEvent.getKey() == eventKeys[6]){
            fight = true;
        }

         */

    }

    @Override
    public void keyReleased (KeyboardEvent keyboardEvent) {

        if(keyboardEvent.getKey() == eventKeys[0]){
            up=false;
            button1=false;
        } if(keyboardEvent.getKey() == eventKeys[1]) {
            down=false;
            button2=false;
        } if(keyboardEvent.getKey() == eventKeys[2]) {
            left=false;
            button3=false;
        } if(keyboardEvent.getKey() == eventKeys[3]) {
            right=false;
            button4=false;
        }

        if (keyboardEvent.getKey() == eventKeys[4]) {
            button1 = false;
        } if (keyboardEvent.getKey() == eventKeys[5]) {

            button2 = false;
        } if (keyboardEvent.getKey() == eventKeys[6]) {

            button3 = false;
        } if (keyboardEvent.getKey() == eventKeys[7]) {
            button4 = false;
        }

        /*
        if(keyboardEvent.getKey() == eventKeys[4]) {
            pause = false;
        }

        if(keyboardEvent.getKey() == eventKeys[5]) {
            resume = false;
        }

        if (keyboardEvent.getKey() == eventKeys[6]){
            fight = false;
        }

         */

    }
}
