package org.academiadecodigo.streetsofcode.Player;

import org.academiadecodigo.streetsofcode.Map.Field;
//import org.academiadecodigo.heroisdovar.Game.GameState;
//import org.academiadecodigo.heroisdovar.UI.Message;
import org.academiadecodigo.streetsofcode.Map.Map;
import org.academiadecodigo.streetsofcode.Tiles.*;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player {

    private Map map;

    private int col;

    private int row;

    private int speed;
    private int updateCounter;
    private int spriteCounter;
    public Rectangle playerTile;

    private String[][] playerSprites;

    private String currentSprite;
    public Picture playerPicture;
    //array de pictures para cada movimento

    private Player player;

    public KeyHandler keyHandler;
    private CheckPlayerCollision checkPlayerCollision;

    private CheckTileCollision checkTileCollision;

    private boolean computerCollision;

    //private Message message;

    private Tile currentTile;

    private Direction currentDirection;

    public Player(Map map, int cols, int rows, Color color, String[][] sprites){
        this.map = map;
        this.checkPlayerCollision = new CheckPlayerCollision(map, this);
        this.checkTileCollision = new CheckTileCollision(this, map);
        this.col = (cols * Field.tileSize) + 10;
        this.row = (rows * Field.tileSize) + 10;
        playerSprites = sprites;
        currentSprite = sprites[0][0];
        playerTile = new Rectangle(col, row, Field.tileSize, Field.tileSize);
        playerPicture = new Picture(col, row, playerSprites[3][0]);
        playerPicture.grow(10, 10);
        playerPicture.translate(0, -10);
        playerTile.setColor(color);
        currentTile = new Tile(new Rectangle(Field.tileSize, Field.tileSize, Field.tileSize, Field.tileSize));
        speed = 7;
        spriteCounter = 0;
        updateCounter = 0;
        //message = new Message();
    }


    public void drawPlayer(){
        playerPicture.delete();
        playerPicture.draw();
    }


    public void setKeyboard(String playerS) {
        keyHandler = new KeyHandler(playerS);
    }


    public void update(){

        if (keyHandler.up) {
            move(Direction.UP);
            currentDirection = Direction.UP;
            checkSprite(0);
        } if (keyHandler.down) {
            move(Direction.DOWN);
            currentDirection = Direction.DOWN;
            checkSprite(3);
        } if (keyHandler.left) {
            move(Direction.LEFT);
            currentDirection = Direction.LEFT;
            checkSprite(2);
        }if (keyHandler.right) {
            move(Direction.RIGHT);
            currentDirection = Direction.RIGHT;
            checkSprite(1);
        }
        //updateCounter++;
        playerPicture.load(currentSprite);

        /*
        if(Game.gameState == GameState.MOVE_PLAYERS) {

        }

         */


    }

    public void checkSprite(int index) {



        spriteCounter++;

        if(spriteCounter > 3) {

            if(updateCounter == 0) {
                currentSprite = playerSprites[index][0];
            } else if(updateCounter == 1) {
                currentSprite = playerSprites[index][1];
            } else if(updateCounter == 2) {
                currentSprite = playerSprites[index][2];
                updateCounter = -1;
            }
            updateCounter++;

            spriteCounter = 0 ;
        }

    }

    public boolean isFighting(){
        //return keyHandler.fight; // ir buscar a key que corresponde ao fight
        // vai ser quando houver uma colisão

        if(checkPlayerCollision.isColliding){
            return true;
        }
        return false; //quando existir collision entre os players vai gerar o cenário do fight
    }



    public void setPlayer(Player player) {
        this.player = player;
    }

    public void move(Direction direction){
        switch (direction){
            case UP:
                playerTile.translate(0, -speed);
                playerPicture.translate(0, -speed);
                if(checkPlayerCollision.up(player) || checkTileCollision.up()){
                    playerTile.translate(0, speed);
                    playerPicture.translate(0, speed);
                    /*
                    if(currentTile.tileType == TileType.YELLOW) {
                        drawMessage();
                    }

                     */
                }

                break;
            case DOWN:
                playerTile.translate(0,speed);
                playerPicture.translate(0, speed);
                computerCollision = false;
                if(checkPlayerCollision.down(player) || checkTileCollision.down()){
                    playerTile.translate(0, -speed);
                    playerPicture.translate(0, -speed);
                }
                break;
            case LEFT:
                playerTile.translate(-speed,0);
                playerPicture.translate(-speed, 0);
                computerCollision = false;
                if(checkPlayerCollision.left(player) || checkTileCollision.left()){
                    playerTile.translate(speed, 0);
                    playerPicture.translate(speed, 0);
                }
                break;
            case RIGHT:
                playerTile.translate(speed,0);
                playerPicture.translate(speed, 0);
                computerCollision = false;
                if(checkPlayerCollision.right(player) || checkTileCollision.right()){
                    playerTile.translate(-speed, 0);
                    playerPicture.translate(-speed, 0);
                }
                break;
        }
    }

    public void setCurrentTile(Tile tile) {
        currentTile = tile;
    }

}


