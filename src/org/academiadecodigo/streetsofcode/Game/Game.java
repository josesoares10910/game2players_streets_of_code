package org.academiadecodigo.streetsofcode.Game;
import org.academiadecodigo.streetsofcode.Fight.HandlerMouse;
import org.academiadecodigo.streetsofcode.Scenes.Scene;
import org.academiadecodigo.streetsofcode.Scenes.SceneManager;
import org.academiadecodigo.streetsofcode.Scenes.ClassRoomScene;
import org.academiadecodigo.streetsofcode.Scenes.FightScene;

public class Game implements Runnable{

    private SceneManager sceneManager;

    private HandlerMouse handlerMouse;

    public Scene currentScene;
    public int currentSceneNum;

    public boolean isOver;

    //public static GameState gameState;
    //public static GameState previousGameState;

    public Game(){

        sceneManager = new SceneManager(2); // criar scene manager
        sceneManager.addScene(new ClassRoomScene(sceneManager)); // adicionar a cena ao game
        sceneManager.addScene(new FightScene(sceneManager)); //todas as vezes que foi criado uma nova cena
        ClassRoomScene cs = (ClassRoomScene) sceneManager.getScenes()[0];
        cs.setFight((FightScene) sceneManager.getScenes()[1]);

        //será necessário de fazer o add da mesma

        HandlerMouse handlerMouse = new HandlerMouse();
        //handlerMouse.mouseClicked();

    }




    public void  run() {

        currentSceneNum = 0;
        sceneManager.loadScene(currentSceneNum); //alterar o zero para um enum ou variavel com todas cenas

        FightScene fightScene = (FightScene) sceneManager.getScenes()[1];

        while (!fightScene.fightIsOver) {
            sceneManager.update();

            draw();

            try{
                Thread.sleep(41);
            } catch(InterruptedException euclides) {

            }
        }
    }


    public void draw(){


    }

    /*
    public void checkGameState() {
        if(player1.keyHandler.pause) {
            if(gameState != GameState.PAUSED) {
                previousGameState = gameState;
                gameState = GameState.PAUSED;
            }
        }

        if(player1.keyHandler.resume) {
                gameState = previousGameState; // questionar se esta parte é especifica para o classroom ou vai ser em todas
                //as cenas
        }
    }

     */
}
