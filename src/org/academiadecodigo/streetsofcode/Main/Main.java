package org.academiadecodigo.streetsofcode.Main;

import org.academiadecodigo.streetsofcode.Game.Game;


public class Main {
    public static void main(String[] args) {

        Game game = new Game();

        game.run();

    }


}
