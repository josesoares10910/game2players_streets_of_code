package org.academiadecodigo.streetsofcode.Scenes;

abstract public class Scene {

    private SceneManager sceneManager;

    public Scene(SceneManager sceneManager){
        this.sceneManager = sceneManager;
    }

    abstract public void load();

    abstract public void unload();

    abstract public void update();

    public SceneManager getSceneManager() {
        return sceneManager;
    }
}
