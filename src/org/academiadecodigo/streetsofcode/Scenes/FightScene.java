package org.academiadecodigo.streetsofcode.Scenes;


import org.academiadecodigo.streetsofcode.Fight.ActionType;
import org.academiadecodigo.streetsofcode.Fight.Fight;
import org.academiadecodigo.streetsofcode.Map.Field;
import org.academiadecodigo.streetsofcode.UI.FightTransition;
import org.academiadecodigo.streetsofcode.UI.FightUI;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class FightScene extends Scene {

    private Rectangle field;
    public FightUI fightUI;

    public Fight fight;

    public boolean fightIsOver;
    FightTransition fightTransition;
    public int currentPlayer, otherPlayer;

    private boolean actionSelected;

    private ActionType currentAnimation;
    public boolean animationIsOver;
    public boolean healthChanged;

    private int counter;




    public FightScene(SceneManager sceneManager) {
        super(sceneManager);
        field = new Rectangle(10, 10, Field.fieldWidth-10, Field.fieldHeight-10);
        fight = new Fight(this);
        fightUI = new FightUI(this);
        fightTransition = new FightTransition();
    }

    @Override
    public void load() { // lógica do fight


        while(fightTransition.startTransition()) {

            fightTransition.transitionSquares[fightTransition.getRow()][fightTransition.getCol()].fill();

            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        fightTransition.deleteTiles();



        field.setColor(Color.BLUE);
        field.fill();
        fightUI.load();

        currentPlayer = (int) (Math.random() * 2);
        otherPlayer = currentPlayer == 0 ? 1 : 0;

        animationIsOver = true;



    }

    @Override
    public void unload() { //delete de elementos visuais
        fightUI.unload();
        field.delete();

    }

    @Override
    public void update() {

        if(fight.players[currentPlayer].name.equals("player1")) {
            fightUI.turnText.setText("Player 2 turn");
            fightUI.button1Text.setText("7: Simple Attack");
            fightUI.button2Text.setText("9: Defense");
            fightUI.button3Text.setText("8: Power Attack");
            fightUI.button4Text.setText("0: Slack Around");
        } else if(fight.players[currentPlayer].name.equals("player2")) {
            fightUI.turnText.setText("Player 1 turn");
            fightUI.button1Text.setText("1: Simple Attack");
            fightUI.button2Text.setText("3: Defense");
            fightUI.button3Text.setText("2: Power Attack");
            fightUI.button4Text.setText("4: Slack Around");
        }

        fightUI.player1HealthText.setText("Player1 Health: " + fight.players[1].health);
        fightUI.player2HealthText.setText("Player2 Health: " + fight.players[0].health);
        fightUI.player1DefenseText.setText("Player1 Defense: " + fight.players[1].defense);
        fightUI.player2DefenseText.setText("Player2 Defense: " + fight.players[0].defense);



        //fightUI.player1Picture.translate(-5, 0);



        if(!actionSelected){
            if(counter == 0) {
                counter = 24;
            }
            counter--;
            fight.currentPlayer = fight.players[currentPlayer];
            if(fight.currentPlayer.hashCode() == fight.players[0].hashCode()) {
                if(counter >= 12) {
                    fightUI.player1Picture.translate(2, 0);
                }
                if(counter < 12) {
                    fightUI.player1Picture.translate(-2, 0);
                }
            } else if(fight.currentPlayer.hashCode() == fight.players[1].hashCode()) {
                if(counter >= 12) {
                    fightUI.player2Picture.translate(2, 0);
                }
                if(counter < 12) {
                    fightUI.player2Picture.translate(-2, 0);
                }
            }

            if (fight.currentPlayer.keyHandler.button1) {
                System.out.println("Button1 pressed");
                currentAnimation = fight.currentPlayer.doAction(ActionType.ATTACK1, otherPlayer);
                animationIsOver = false;
                actionSelected = true;
                healthChanged = true;

            } else if (fight.currentPlayer.keyHandler.button2) {
                System.out.println("Button2 pressed");
                currentAnimation = fight.currentPlayer.doAction(ActionType.ATTACK2, otherPlayer);
                animationIsOver = false;
                actionSelected = true;
                healthChanged = true;

            } else if (fight.currentPlayer.keyHandler.button3) {
                System.out.println("Button3 pressed");
                currentAnimation = fight.currentPlayer.doAction(ActionType.DEFEND, currentPlayer);
                animationIsOver = false;
                actionSelected = true;

            } else if (fight.currentPlayer.keyHandler.button4) {
                System.out.println("Button4 pressed");
                currentAnimation = fight.currentPlayer.doAction(ActionType.RANDOM, otherPlayer);
                animationIsOver = false;
                actionSelected = true;
            }

        }

        if(!animationIsOver){
            fightUI.animate(currentAnimation);
        }


        if(healthChanged) {
            fightUI.updateHealth(fight.players[otherPlayer]);
            healthChanged = false;
        }


        if(animationIsOver && actionSelected){
            currentPlayer = otherPlayer;
            otherPlayer = currentPlayer == 0 ? 1 : 0;
            actionSelected = false;
        }


        if(animationIsOver && fight.isFinished()){

            fightUI.exitText.setColor(Color.WHITE);
            fightUI.exitText.setText("Press Esc to Exit");
            fightUI.exitText.grow(90, 20);
            fightUI.exitText.translate(0, -50);
            if(fight.players[0].health <= 0) {
                fightUI.playerWon.load("/fightSprites/player_1_won.png");
            } else {
                fightUI.playerWon.load("/fightSprites/player_2_won.png");
            }
            fightUI.playerWon.draw();
            fightUI.exitText.draw();
            fightIsOver = true;
            //unload();
            //getSceneManager().loadScene(0); // mostrar alguma informação antes de mudar o cenário
            return;
        }







    }
}
