package org.academiadecodigo.streetsofcode.Scenes;

public class SceneManager { // gerir todas as cenas(menu inicial, sala, modo fight)

    private Scene[] scenes;

    private Scene currentScene;

    public SceneManager(int qntyScene){
        scenes = new Scene[qntyScene];
    }

    public void addScene(Scene newScene){
        for(int i = 0; i < scenes.length; i++){
            Scene scene = scenes[i];
            if(scene == null){ //verificar se naquela posição existe alguma cena
                scenes[i] = newScene; // adicionar nesse espaço vazio a nova cena
                return;
            }
        }
        throw new RuntimeException("SceneManager is full");
    }

    public void loadScene(int index){
        if(currentScene != null){
            currentScene.unload(); // quando tiver uma cena carregada dar unload
        }
        currentScene = scenes[index]; // o current scene vai buscar a cena ao index passado no parametro
        currentScene.load(); // depois vai fazer o load do mesmo

    }

    public void update(){
        currentScene.update();
    }

    public Scene[] getScenes() {
        return scenes;
    }
}
